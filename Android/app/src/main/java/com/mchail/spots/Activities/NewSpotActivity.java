package com.mchail.spots.Activities;

import android.app.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import com.google.android.gms.maps.model.*;
import com.mchail.spots.*;
import com.mchail.spots.Database.*;

/**
 * Created by kostello on 2/16/2015.
 */
public class NewSpotActivity extends Activity {

    private LatLng spotLatLng;
    public static final String SPOT_LAT_LNG_KEY = "spot_lat_lng";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_spot_activity);

        spotLatLng = getIntent().getExtras().getParcelable(SPOT_LAT_LNG_KEY);
    }

    public void createNewSpot(View view){

        EditText nameET = (EditText)findViewById(R.id.name);
        EditText descriptionET = (EditText)findViewById(R.id.description);
        EditText timeET = (EditText)findViewById(R.id.time);
        EditText priceET = (EditText)findViewById(R.id.price);
        EditText tagsET = (EditText)findViewById(R.id.tags);
        
        final String spotName = nameET.getText().toString();
        final String spotDescription = descriptionET.getText().toString();
        final String spotTime = timeET.getText().toString();
        final String spotPrice = priceET.getText().toString();
        final String spotTags = tagsET.getText().toString();


        new Thread(new Runnable(){
            @Override
            public void run() {
				OnlineDatabase.createSpot(getApplicationContext(), spotLatLng, spotName, spotDescription, spotTime, spotPrice, spotTags);
				
				runOnUiThread(new Runnable(){
						@Override
						public void run()
						{
							finish();
						}
				});
            }
			
			
        }).start();




    }
}
