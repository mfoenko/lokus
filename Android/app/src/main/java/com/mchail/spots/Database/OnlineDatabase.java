package com.mchail.spots.Database;

import android.content.*;
import android.preference.*;
import android.util.*;
import com.google.android.gms.maps.model.*;
import java.util.*;
import org.apache.http.*;
import org.apache.http.client.*;
import org.apache.http.client.entity.*;
import org.apache.http.client.methods.*;
import org.apache.http.impl.client.*;
import org.apache.http.message.*;

public class OnlineDatabase
{

    public static final String GET_ALL_SPOTS_URL = "http://www.mchail.byethost4.com/Lokus/Web/scripts/php/get_all_spots.php";
	public static final String GET_USER_URL = "http://www.mchail.byethost4.com/Lokus/Web/scripts/php/get_user.php";
	public static final String LOGIN_URL = "http://www.mchail.byethost4.com/Lokus/Web/scripts/php/login.php";
	public static final String REGISTER_URL = "http://www.mchail.byethost4.com/Lokus/Web/scripts/php/register.php";
    public static final String CREATE_SPOT_URL = "http://www.mchail.byethost4.com/Lokus/Web/scripts/php/new_spot.php";
    public static final String GET_SPOT_URL = "http://www.mchail.byethost4.com/Lokus/Web/scripts/php/get_spot.php";

	public static final String PREF_KEY_LOGGED_IN_USER_ID = "pref_logged_in_user";
	public static final String PREF_KEY_LOGGED_IN_USERNAME = "pref_logged_in_username";
	public static final String PREF_KEY_LOGGED_IN_PASSWORD = "pref_logged_in_password";
	
	public static final long REGISTER_RESPONSE_USERNAME_EXISTS = -2;

	private static User loggedInUser;


	public static ArrayList<Spot> getAllSpots()
	{

		ArrayList<Spot> spotsList = new ArrayList<>();
		HttpClient client = new DefaultHttpClient();
        HttpUriRequest request = new HttpGet(GET_ALL_SPOTS_URL);
		ResponseHandler<String> handler = new BasicResponseHandler();

		String response = "";

		try
		{
			response = client.execute(request, handler);
			System.out.println(response);
		}
		catch (Exception e)
		{
            e.printStackTrace();
		}


		String[] spotStrings = response.split(";");
		for (String spots:spotStrings)
		{
			String[] spotParams = spots.split(",");
			try
			{
				spotsList.add(new Spot(Long.parseLong(spotParams[0]), spotParams[1], spotParams[2], Double.parseDouble(spotParams[3]), Double.parseDouble(spotParams[4]))); 
			}
			catch (NumberFormatException e)
			{
				e.printStackTrace();
			}
		}
		return spotsList;
	}

	public static long logIn(Context c, String user, String pass)
	{

		long userId = -1;

		HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(LOGIN_URL);

		List<NameValuePair> fields = new ArrayList<>();
		fields.add(new BasicNameValuePair("username", user));
		fields.add(new BasicNameValuePair("password", pass));



		ResponseHandler<String> handler = new BasicResponseHandler();

		String response;

		try
		{
			post.setEntity(new UrlEncodedFormEntity(fields));

			response = client.execute(post, handler);
			System.out.println(response);

			userId = Long.parseLong(response);
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
			prefs.edit().putLong(PREF_KEY_LOGGED_IN_USER_ID, userId).putString(PREF_KEY_LOGGED_IN_USERNAME, user).putString(PREF_KEY_LOGGED_IN_PASSWORD, pass).apply();
            loggedInUser = getUser(userId);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}


		return userId;

	}

    public static void logout(Context c){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        prefs.edit().putLong(PREF_KEY_LOGGED_IN_USER_ID, -1)
		.putString(PREF_KEY_LOGGED_IN_USERNAME, "")
		.putString(PREF_KEY_LOGGED_IN_PASSWORD, "")
		.apply();
		
        loggedInUser = null;

    }

	public static User getUser(long id)
	{
		User user = null;

		HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(GET_USER_URL + "?" + "user_id=" + id);
		ResponseHandler<String> handler = new BasicResponseHandler();

		String response;

		try
		{
			response = client.execute(request, handler);
			System.out.println(response);


			String[] userParams = response.split(",");
			user = new User(Long.parseLong(userParams[0]), userParams[1], userParams[2]);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}


		return user;

	}

	public static long registerAndLogin(Context c, String username, String pass, String name)
	{
		long user = -1;

		HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(REGISTER_URL);

		List<NameValuePair> fields = new ArrayList<>();
		fields.add(new BasicNameValuePair("username", username));
		fields.add(new BasicNameValuePair("password", pass));
		fields.add(new BasicNameValuePair("name", name));



		ResponseHandler<String> handler = new BasicResponseHandler();

		String response;

		try
		{
			post.setEntity(new UrlEncodedFormEntity(fields));

			response = client.execute(post, handler);
			System.out.println("register "+response);

			long responseCode = Long.parseLong(response);
			if (responseCode > 0)
			{
				user = logIn(c, username, pass);
			}
			else if (responseCode == REGISTER_RESPONSE_USERNAME_EXISTS)
			{
				user = -2;				
			}




		}
		catch (Exception e)
		{
			e.printStackTrace();
		}


		return user;

	}

    public static void createSpot(Context c, LatLng latlng, String spotName, String spotDescription, String time, String price, String tags){
        HttpClient client = new DefaultHttpClient();
        HttpPost request = new HttpPost(CREATE_SPOT_URL);
		List<NameValuePair> pairs = new ArrayList<>();
		pairs.add(new BasicNameValuePair("user", ""+getLoggedInUser(c).getId()));
		pairs.add(new BasicNameValuePair("name", spotName));
		pairs.add(new BasicNameValuePair("description", spotDescription));
		pairs.add(new BasicNameValuePair("latitude", ""+latlng.latitude));
		pairs.add(new BasicNameValuePair("longitude", ""+latlng.longitude));
        pairs.add(new BasicNameValuePair("time", time));
        pairs.add(new BasicNameValuePair("price", price));
        pairs.add(new BasicNameValuePair("tags", tags));
		pairs.add(new BasicNameValuePair("password",PreferenceManager.getDefaultSharedPreferences(c).getString(PREF_KEY_LOGGED_IN_PASSWORD, "")));
		pairs.add(new BasicNameValuePair("username",PreferenceManager.getDefaultSharedPreferences(c).getString(PREF_KEY_LOGGED_IN_USERNAME, "")));

		
		ResponseHandler<String> handler = new BasicResponseHandler();
        try{
			request.setEntity(new UrlEncodedFormEntity(pairs));
            Log.i("AAA",client.execute(request, handler));
        }catch(Exception e){
			e.printStackTrace();
        }

    }

	public static User getLoggedInUser(Context c)
	{
        if(loggedInUser == null){
			final long id = PreferenceManager.getDefaultSharedPreferences(c).getLong(PREF_KEY_LOGGED_IN_USER_ID, -1);
            new Thread(new Runnable(){
                @Override
                public void run() {
                    loggedInUser = getUser(id);
                }
            }).start();

        }
		return loggedInUser;
	}


}
