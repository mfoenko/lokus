package com.mchail.spots.Activities;

import android.content.*;
import android.location.*;
import android.os.*;
import android.support.v4.app.*;
import android.view.*;
import android.widget.*;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;
import com.mchail.spots.*;
import com.mchail.spots.Database.*;
import com.mchail.spots.Misc.*;
import java.util.*;

public class MapsActivity extends FragmentActivity implements GoogleMap.OnMapLongClickListener, GoogleMap.InfoWindowAdapter, GoogleMap.OnInfoWindowClickListener
{

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private Marker mMarker;
	private ArrayList<Spot> mSpots = new ArrayList<Spot>();
	
	private static final float DEFAULT_ZOOM_LEVEL = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState)
	{
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
		Thread.setDefaultUncaughtExceptionHandler(new UncaughtException(MapsActivity.this));
		
        setUpMapIfNeeded();
        mMap.setOnMapLongClickListener(this);
        mMap.setInfoWindowAdapter(this);
        mMap.setOnInfoWindowClickListener(this);
		new Thread(new Runnable(){

				@Override
				public void run()
				{
					mSpots = OnlineDatabase.getAllSpots();
                    OnlineDatabase.getLoggedInUser(getApplicationContext());
					runOnUiThread(new Runnable(){

							@Override
							public void run()
							{
								displaySpots();
								
							}
							
						
					});
					
				}

				
			}).start();
	}

    @Override
    protected void onResume()
	{
        super.onResume();
        setUpMapIfNeeded();
		new Thread(new Runnable(){

				@Override
				public void run()
				{
					mSpots = OnlineDatabase.getAllSpots();
                    OnlineDatabase.getLoggedInUser(getApplicationContext());
					runOnUiThread(new Runnable(){

							@Override
							public void run()
							{
								displaySpots();

							}


						});

				}


			}).start();
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.user, menu);
		
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch(item.getItemId()){
			case R.id.user:
                User loggedInUser = OnlineDatabase.getLoggedInUser(getApplicationContext());
                if(loggedInUser == null){
                    Intent i = new Intent(this, LoginActivity.class);
                    startActivity(i);
                }else{
                    Intent i = new Intent(this, UserProfileActivity.class);
                    i.putExtra(UserProfileActivity.USER_ID_KEY, loggedInUser.getId());
                    startActivity(i);
                }

				break;
		}
		return super.onOptionsItemSelected(item);
	}

    private void setUpMapIfNeeded()
	{
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null)
		{
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
			mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
			LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
			Location loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			if(loc != null)
				mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(loc.getLatitude(), loc.getLongitude()), DEFAULT_ZOOM_LEVEL));
			mMap.setInfoWindowAdapter(this);
            mMap.setOnInfoWindowClickListener(this);
        }
    }


	public void displaySpots()
	{

		mMap.clear();
		

		for (Spot spot:mSpots)
		{
			mMap.addMarker(new MarkerOptions().position(spot.coordinates).title(spot.name).snippet(spot.user));
		}
	}

	@Override
	public void onMapLongClick(LatLng latLng)
	{
		if (mMarker != null)
		{
			mMarker.remove();
		}
		mMarker = mMap.addMarker(new MarkerOptions().position(latLng).title("my marker"));

        mMarker.showInfoWindow();
	}

    @Override
    public View getInfoWindow(Marker marker) {

       return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        if(marker.equals(mMarker)){
            ImageView plusIv = new ImageView(this);
            plusIv.setImageResource(R.drawable.ic_action_new);
            return plusIv;

        }
        return null;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
		if(marker.equals(mMarker)){
			if(OnlineDatabase.getLoggedInUser(this) != null){
				Intent i = new Intent(this, NewSpotActivity.class);
				i.putExtra(NewSpotActivity.SPOT_LAT_LNG_KEY, marker.getPosition());
				startActivity(i);
				
			}else{
				Intent i = new Intent(this, LoginActivity.class);
				startActivity(i);
				
				
			}
			
		}
    }
}
