/*
 * Decompiled with CFR 0_92.
 * 
 * Could not load the following classes:
 *  android.app.AlertDialog
 *  android.app.AlertDialog$Builder
 *  android.content.Context
 *  android.content.DialogInterface
 *  android.content.DialogInterface$OnClickListener
 *  android.content.Intent
 *  android.content.pm.PackageInfo
 *  android.content.pm.PackageManager
 *  android.os.Build
 *  android.os.Build$VERSION
 *  android.os.Environment
 *  android.os.Looper
 *  android.os.StatFs
 *  android.util.Log
 *  java.io.File
 *  java.io.PrintWriter
 *  java.io.StringWriter
 *  java.io.Writer
 *  java.lang.CharSequence
 *  java.lang.Class
 *  java.lang.ClassNotFoundException
 *  java.lang.Exception
 *  java.lang.Math
 *  java.lang.NoClassDefFoundError
 *  java.lang.Object
 *  java.lang.Override
 *  java.lang.String
 *  java.lang.StringBuffer
 *  java.lang.StringBuilder
 *  java.lang.System
 *  java.lang.Thread
 *  java.lang.Thread$UncaughtExceptionHandler
 *  java.lang.Throwable
 *  java.util.Date
 *  java.util.Locale
 */
package com.mchail.spots.Misc;
import android.app.*;
import android.content.*;
import android.content.pm.*;
import android.os.*;
import android.util.*;
import com.google.android.gms.internal.*;
import java.io.*;
import java.util.*;

public class UncaughtException implements Thread.UncaughtExceptionHandler
{


    private Context context;
    private static Context context1;

    public UncaughtException(Context ctx)
	{
        context = ctx;
        context1 = ctx;
    }

    private StatFs getStatFs()
	{
        File path = Environment.getDataDirectory();
        return new StatFs(path.getPath());
    }

    private long getAvailableInternalMemorySize(StatFs stat)
	{
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        return availableBlocks * blockSize;
    }

    private long getTotalInternalMemorySize(StatFs stat)
	{
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        return totalBlocks * blockSize;
    }

    private void addInformation(StringBuilder message)
	{
        message.append("Locale: ").append(Locale.getDefault()).append('\n');
        try
		{
            PackageManager pm = context.getPackageManager();
            PackageInfo pi;
            pi = pm.getPackageInfo(context.getPackageName(), 0);
            message.append("Version: ").append(pi.versionName).append('\n');
            message.append("Package: ").append(pi.packageName).append('\n');
        }
		catch (Exception e)
		{
            Log.e("CustomExceptionHandler", "Error", e);
            message.append("Could not get Version <span id='IL_AD6' class='IL_AD'>information</span> for ").append(
				context.getPackageName());
        }
        message.append("Phone <span id='IL_AD12' class='IL_AD'>Model</span>: ").append(android.os.Build.MODEL)
			.append('\n');
        message.append("Android Version: ")
			.append(android.os.Build.VERSION.RELEASE).append('\n');
        message.append("Board: ").append(android.os.Build.BOARD).append('\n');
        message.append("Brand: ").append(android.os.Build.BRAND).append('\n');
        message.append("Device: ").append(android.os.Build.DEVICE).append('\n');
        message.append("Host: ").append(android.os.Build.HOST).append('\n');
        message.append("ID: ").append(android.os.Build.ID).append('\n');
        message.append("Model: ").append(android.os.Build.MODEL).append('\n');
        message.append("Product: ").append(android.os.Build.PRODUCT)
			.append('\n');
        message.append("Type: ").append(android.os.Build.TYPE).append('\n');
        StatFs stat = getStatFs();
        message.append("Total Internal memory: ")
			.append(getTotalInternalMemorySize(stat)).append('\n');
        message.append("Available Internal memory: ")
			.append(getAvailableInternalMemorySize(stat)).append('\n');
    }

    public void uncaughtException(Thread t, Throwable e)
	{
        try
		{
            StringBuilder report = new StringBuilder();
            Date curDate = new Date();
            report.append("Error Report collected on : ")
				.append(curDate.toString()).append('\n').append('\n');
            report.append("<span id='IL_AD10' class='IL_AD'>Informations</span> :").append('\n');
            addInformation(report);
            report.append('\n').append('\n');
            report.append("Stack:\n");
            final Writer result = new StringWriter();
            final PrintWriter printWriter = new PrintWriter(result);
            e.printStackTrace(printWriter);
            report.append(result.toString());
            printWriter.close();
            report.append('\n');
            report.append("**** End of current Report ***");
            Log.e(UncaughtException.class.getName(),
				  "Error while sendErrorMail" + report);
            sendErrorMail(report);
        }
		catch (Exception ignore)
		{
			Log.e("AAA", ignore.toString());
            Log.e(UncaughtException.class.getName(),
				  "Error while sending error e-mail", ignore);
        }
    }

    /**
     * <span id="IL_AD3" class="IL_AD">This method</span> for call alert dialog when <span id="IL_AD4" class="IL_AD">application</span> crashed!
     */
    public void sendErrorMail(final StringBuilder errorContent)
	{
		Intent sendIntent = new Intent(
			Intent.ACTION_SEND);
		String subject = "Crash Report";
		StringBuilder body = new StringBuilder();
		body.append('\n').append('\n');
		body.append(errorContent).append('\n')
			.append('\n');
		// sendIntent.setType("text/plain");
		sendIntent.setType("message/rfc822");
		sendIntent.putExtra(Intent.EXTRA_EMAIL,
							new String[] { "mfoenko@gmail.com" });
		sendIntent.putExtra(Intent.EXTRA_TEXT,
							body.toString());
		sendIntent.putExtra(Intent.EXTRA_SUBJECT,
							subject);
		sendIntent.setType("message/rfc822");
		context1.startActivity(sendIntent);
		System.exit(0);
	}
	
}
