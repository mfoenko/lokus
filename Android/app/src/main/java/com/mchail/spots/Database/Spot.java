package com.mchail.spots.Database;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by mfoenko on 2/15/2015.
 */
public class Spot {

    private long id;
    private LatLng coordinates;
    private String name;
    private User author;
    private String time;
    private String price;
    private String tags;

    public Spot(long id, long userId, String name, double lat, double lng, String time, String price, String tags){
        this.id = id;
        this.coordinates = new LatLng(lat, lng);
        this.name = name;

    }

}
