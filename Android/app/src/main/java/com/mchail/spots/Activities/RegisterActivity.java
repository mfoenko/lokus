package com.mchail.spots.Activities;

import android.app.*;
import android.content.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import com.mchail.spots.*;
import com.mchail.spots.Database.*;

public class RegisterActivity extends Activity
{

	public static final String RESULT_EXTRA_USER_ID = "user_id";
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register_activity);
	}
	
	public void register(View view){
		
		final TextView usernameET = (TextView)findViewById(R.id.username);
		final TextView passwordET = (TextView)findViewById(R.id.password);
		final TextView nameET = (TextView)findViewById(R.id.name);
		final Button button = (Button)view;

		final Context context = view.getContext();
		final String username = usernameET.getText().toString();
		final String password = passwordET.getText().toString();
		final String name = nameET.getText().toString();

		usernameET.setEnabled(false);
		passwordET.setEnabled(false);
		nameET.setEnabled(false);
		button.setEnabled(false);


		new Thread(new Runnable(){

				@Override
				public void run()
				{
					final long userId = OnlineDatabase.registerAndLogin(context, username, password, name);
					final User user = OnlineDatabase.getUser(userId);


					runOnUiThread(new Runnable(){

							@Override
							public void run()
							{
								usernameET.setEnabled(true);
								passwordET.setEnabled(true);
								nameET.setEnabled(true);
								button.setEnabled(true);

								if(userId > 0){
									Intent i = new Intent();
									i.putExtra(RESULT_EXTRA_USER_ID, userId);
									setResult(RESULT_OK, i);
									finish();
									Toast.makeText(context,"Logged in as "+user.getName(), 250).show();
									

								}else{
									if(userId == -2){
										Toast.makeText(context, "Username Taken", 250).show(); 
										
									}
									if(userId==-1){
										Toast.makeText(context, "Server Error", 250).show(); 
										
									}
									passwordET.setText("");
									}

							}


						});

				}

			}).start();
		
		
	}
	
	
	
}
