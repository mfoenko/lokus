/*
 * Decompiled with CFR 0_92.
 * 
 * Could not load the following classes:
 *  android.content.Context
 *  android.content.Intent
 *  android.content.pm.PackageInfo
 *  android.content.pm.PackageManager
 *  android.os.Build
 *  android.os.Build$VERSION
 *  android.os.Environment
 *  android.os.StatFs
 *  android.util.Log
 *  java.io.File
 *  java.lang.Exception
 *  java.lang.Object
 *  java.lang.String
 *  java.lang.StringBuilder
 *  java.lang.Throwable
 *  java.util.Locale
 */
package com.mchail.spots.Misc;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;
import java.io.File;
import java.util.Locale;

public class EmailDev {
    public static final int BUG_REPORT = 1;
    public static final int SUGGESTION = 2;

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     */
    public EmailDev(Context context, int n) {
        Intent intent = new Intent("android.intent.action.SEND");
        String string = "";
        StringBuilder stringBuilder = new StringBuilder();
        switch (n) {
            case 1: {
                string = "I found a bug!";
                stringBuilder.append("Describe the problem below: \n If your problem is a crash, use the Report button in the dialog instead");
                stringBuilder.append('\n').append('\n').append('\n').append('\n').append('\n').append('\n').append('\n').append('\n').append('\n').append('\n').append('\n').append('\n').append('\n').append('\n');
                stringBuilder.append("Locale: ").append((Object)Locale.getDefault()).append('\n');
                try {
                    PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                    stringBuilder.append("Version: ").append(packageInfo.versionName).append('\n');
                    stringBuilder.append("Package: ").append(packageInfo.packageName).append('\n');
                }
                catch (Exception var15_8) {
                    Log.e((String)"CustomExceptionHandler", (String)"Error", (Throwable)var15_8);
                    stringBuilder.append("Could not get Version information for ").append(context.getPackageName());
                }
                stringBuilder.append("Phone Model: ").append(Build.MODEL).append('\n');
                stringBuilder.append("Android Version: ").append(Build.VERSION.RELEASE).append('\n');
                stringBuilder.append("Board: ").append(Build.BOARD).append('\n');
                stringBuilder.append("Brand: ").append(Build.BRAND).append('\n');
                stringBuilder.append("Device: ").append(Build.DEVICE).append('\n');
                stringBuilder.append("Host: ").append(Build.HOST).append('\n');
                stringBuilder.append("ID: ").append(Build.ID).append('\n');
                stringBuilder.append("Model: ").append(Build.MODEL).append('\n');
                stringBuilder.append("Product: ").append(Build.PRODUCT).append('\n');
                stringBuilder.append("Type: ").append(Build.TYPE).append('\n');
                StatFs statFs = this.getStatFs();
                stringBuilder.append("Total Internal memory: ").append(this.getTotalInternalMemorySize(statFs)).append('\n');
                stringBuilder.append("Available Internal memory: ").append(this.getAvailableInternalMemorySize(statFs));
            }
            default: {
                break;
            }
            case 2: {
                string = "I have an idea for Ultradex!";
                stringBuilder.append("Describe your idea(s) below:");
                stringBuilder.append('\n').append('\n').append('\n').append('\n').append('\n').append('\n').append('\n').append('\n').append('\n').append('\n');
            }
        }
        intent.setType("message/rfc822");
        intent.putExtra("android.intent.extra.EMAIL", new String[]{"UltradexApp@gmail.com"});
        intent.putExtra("android.intent.extra.TEXT", stringBuilder.toString());
        intent.putExtra("android.intent.extra.SUBJECT", string);
        context.startActivity(intent);
    }

    private long getAvailableInternalMemorySize(StatFs statFs) {
        return (long)statFs.getBlockSize() * (long)statFs.getAvailableBlocks();
    }

    private StatFs getStatFs() {
        File file = Environment.getDataDirectory();
        StatFs statFs = new StatFs(file.getPath());
        return statFs;
    }

    private long getTotalInternalMemorySize(StatFs statFs) {
        return (long)statFs.getBlockSize() * (long)statFs.getBlockCount();
    }
}

