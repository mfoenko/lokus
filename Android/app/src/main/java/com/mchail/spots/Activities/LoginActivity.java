package com.mchail.spots.Activities;

import android.app.*;
import android.content.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import com.mchail.spots.*;
import com.mchail.spots.Database.*;

public class LoginActivity extends Activity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_activity);
		
		
	}
	
	public void login(View view){
		
		final TextView usernameET = (TextView)findViewById(R.id.username);
		final TextView passwordET = (TextView)findViewById(R.id.password);
		final Button button = (Button)view;
		
		final Context context = view.getContext();
		final String username = usernameET.getText().toString();
		final String password = passwordET.getText().toString();
		
		
		usernameET.setEnabled(false);
		passwordET.setEnabled(false);
		button.setEnabled(false);
		
		
		new Thread(new Runnable(){

				@Override
				public void run()
				{
					final long userId = OnlineDatabase.logIn(context, username, password);
					final User user = OnlineDatabase.getUser(userId);
					
						
					runOnUiThread(new Runnable(){

							@Override
							public void run()
							{
								usernameET.setEnabled(true);
								passwordET.setEnabled(true);
								button.setEnabled(true);
								
								if(user != null){
									finish();
									Toast.makeText(context,"Logged in as "+user.getName(), 250).show(); 
									
								}else{
									passwordET.setText("");
									Toast.makeText(context, "Invalid Username or Password", 250).show(); 
								}
								
							}

						
					});
					
				}

		}).start();
		
		
	}
	
	
	public void openRegister(View view){
		Intent i = new Intent(this, RegisterActivity.class);
		startActivityForResult(i, 0);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if(resultCode == RESULT_OK && data.getExtras().getLong(RegisterActivity.RESULT_EXTRA_USER_ID) > 0){
			finish();
		}
		
	}
	
	
	
}
