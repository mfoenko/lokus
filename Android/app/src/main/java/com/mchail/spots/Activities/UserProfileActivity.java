package com.mchail.spots.Activities;

import android.app.*;
import android.os.*;

import com.mchail.spots.*;
import com.mchail.spots.Database.*;

import android.widget.*;
import android.view.*;

public class UserProfileActivity extends Activity {

    public static final String USER_ID_KEY = "user_id";
    public Thread getUserThread;
    private User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile_activity);

        Bundle extras = getIntent().getExtras();
        final long userId = extras.getLong(USER_ID_KEY);
        new Thread(new Runnable() {
            @Override
            public void run() {
                mUser = OnlineDatabase.getUser(userId);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        TextView nameTV = (TextView) findViewById(R.id.name);
                        TextView usernameTV = (TextView) findViewById(R.id.username);

                        nameTV.setText(mUser.getName());
                        usernameTV.setText(mUser.getUsername());
                        invalidateOptionsMenu();

                    }

                });

            }

        }).start();

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        final MenuInflater inflater = getMenuInflater();
        if (mUser == null) {
            return false;
        }

        if (mUser.getId() == OnlineDatabase.getLoggedInUser(getApplicationContext()).getId()) {
            inflater.inflate(R.menu.logout, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                OnlineDatabase.logout(this);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
